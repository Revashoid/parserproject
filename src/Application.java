import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Application {
    public static void main(String[] args) {
        String path = "src/Resources/data.csv";

        Parser parser = new Parser();
        List<String> res = parser.parsing(path);
        res.remove(0); //удалить 1 элемент списка (столбец с описанием)

        Map<Date, Date> Dates = parser.takeColumns(res, 10,11);
        /* Dates.forEach((k,v)-> System.out.println("FirstDate: " + k + " LastDate: " + v)); */

        Date userDate = parser.setDate();
        AtomicInteger counter = parser.takeCount(Dates,userDate);
        System.out.println("Count of restrictions on: " + userDate + " is: " + counter);
    }
}
