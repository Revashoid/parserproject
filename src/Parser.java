import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class Parser {

    public Date setDate() {
        System.out.println("Enter a date like 2012/02/05");
        Scanner scanner = new Scanner(System.in);
        String strdate = scanner.nextLine();
        SimpleDateFormat s = new SimpleDateFormat("yyyy/MM/dd");
        Date userDate = null;
        try {
            userDate = s.parse(strdate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        scanner.close();
        return userDate;
    }

    public List<String> parsing(String path) {
        ArrayList<String> res = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line;
            while ((line = reader.readLine()) != null) {
                res.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public Map<Date, Date> takeColumns(List<String> stringList, int col1, int col2){
        Map<Date, Date> Dates = new HashMap<>(stringList.size()/2);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");

        for(String item:stringList) {
            String[] splitted = item.split(",(?=([^\"]|\"[^\"]*\")*$)");
            try {
                Dates.put(formatter.parse(splitted[col1]), formatter.parse(splitted[col2]));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        return Dates;
    }

    public AtomicInteger takeCount(Map<Date, Date> Dates, Date userDate){
        AtomicInteger counter = new AtomicInteger();
        Dates.forEach((k,v)->{
            if((k.equals(userDate)) || (v.equals(userDate)) || (userDate.before(v) && userDate.after(k))){
                counter.getAndIncrement();
            }
        });
        return counter;
    }
}